import java.util.Collection;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MapCollection {
    
    public static List<Integer> doubleCollection(List<Integer> list) {
        return list.stream().map(number -> number * 2).collect(Collectors.toList());
    }
    
    public static List<String> mapToStringCollection(List<Integer> list) {
        return list.stream().map(number -> Character.toString((char)(number + 96))).collect(Collectors.toList());
    }
    
    public static List<String> uppercaseCollection(List<String> list) {
        return list.stream().map(String::toUpperCase).collect(Collectors.toList());
    }
    
    public static List<Integer> transformTwoDimensionalToOne(List<List<Integer>> list) {
        return list.stream().flatMap(Collection::stream).collect(Collectors.toList());
    }
}
