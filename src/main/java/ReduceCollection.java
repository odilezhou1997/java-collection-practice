import java.util.List;
import java.util.stream.Collectors;

public class ReduceCollection {
    public static int getMax(List<Integer> list) {
        return list.stream().reduce(0 , Integer::max);
    }
    
    public static double getAverage(List<Integer> list) {
        return list.stream().reduce(0, Integer::sum) / list.size();
    }
    
    public static int getSum(List<Integer> list) {
        return list.stream().reduce(0, Integer::sum);
    }
    
    public static double getMedian(List<Integer> list) {
        if (list.size() % 2 != 0)
            return list.get((int)Math.floor(list.size()/2));
        else
            return 0.5 * (list.get(list.size()/2) + list.get(list.size()/2 - 1));
    }
    
    public static int getFirstEven(List<Integer> list) {
        List<Integer> newList = list.stream().filter(number -> number % 2 == 0).collect(Collectors.toList());
        return newList.get(0);
    }
}
