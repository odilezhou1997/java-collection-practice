import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CollectionOperation {
    
    public static List<Integer> getListByInterval(int left, int right) {
        return left < right
                ? Stream.iterate(left, n -> n + 1).limit(right).collect(Collectors.toList())
                : Stream.iterate(left, n -> n - 1).limit(left).collect(Collectors.toList());
    }
    
    
    public static List<Integer> removeLastElement(List<Integer> list) {
        return list.stream().limit(list.size() - 1).collect(Collectors.toList());
    }
    
    public static List<Integer> sortDesc(List<Integer> list) {
        return list.stream().sorted(Comparator.comparing(Integer::intValue).reversed()).collect(Collectors.toList());
    }
    
    
    public static List<Integer> reverseList(List<Integer> list) {
        Collections.reverse(list);
        return list;
    }
    
    
    public static List<Integer> concat(List<Integer> list1, List<Integer> list2) {
        return Stream.concat(list1.stream(), list2.stream()).collect(Collectors.toList());
    }
    
    public static List<Integer> union(List<Integer> list1, List<Integer> list2) {
        return Stream.concat(list1.stream(), list2.stream()).distinct().collect(Collectors.toList());
    }
    
    public static boolean isAllElementsEqual(List<Integer> list1, List<Integer> list2) {
        return Stream.concat(list1.stream(), list2.stream())
                .noneMatch(number -> !list1.contains(number) || !list2.contains(number));
    }
}
